# BASH PASSWORD GENERATOR

Generates a secure password from command line or from within another bash script.

## The Password Generator Goals

The generated password:

* As a minimal length of 16 characters.
* Is composed by:
    + lower case letters.
    + upper case letters.
    + numbers.
    + special characters.

The script will:

* Allow to specify the password length.
* Run in quite mode.

## How to Install

Using the [Bash Package Manager](https://gitlab.com/exadra37-bash/package-manager) just go to the folder where you want it 
installed and type:

```
$ bpm require exadra37-bash/generators password last-stable-release gitlab.com
```


## How to Use

```bash
$ ./password

---> PASSWORD GENETRATOR <---

a[S]m7kp}rOd+*'5
```

### Quiet Option 

```bash
$ ./password --quiet
a[S]m7kp}rOd+*'5
```

```bash
$ ./password -q
a[S]m7kp}rOd+*'5
```

#### Lenght Option

```bash
$ ./password --lenght 32

---> PASSWORD GENETRATOR <---

w&/A=jrO~K";{liv$WGurdf!dbhbkfyk
```

```bash
$ ./password -l 32

---> PASSWORD GENETRATOR <---

w&/A=jrO~K";{liv$WGurdf!dbhbkfyk
```
